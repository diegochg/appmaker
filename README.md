# Archetype Gradle

Official documentation [gradle-archetype-plugin](https://github.com/orctom/gradle-archetype-plugin)

Command Example: 
gradle cleanArch generate -i -Dtarget=generated -Dgroup=com.tedregal.siteapp -Dname=site-app -Dversion=0.0.1-SNAPSHOT

Copy generated project from generated to another location and read its README.md file
