package @group@.base;

import io.restassured.RestAssured;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import org.junit.platform.commons.util.StringUtils;

public class ApiTestBase {

  private static String DEFAULT_LOCALHOST_VALUE = "";

  public ApiTestBase() {
    String apiUrl = System.getProperty("api.url");
    if (StringUtils.isBlank(apiUrl)) {
      apiUrl = DEFAULT_LOCALHOST_VALUE;
    }
    if (isInvalidUrl(apiUrl)) {
      throw new IllegalArgumentException("Url has not valid content.");
    }
    RestAssured.baseURI = apiUrl;
  }

  private boolean isInvalidUrl(String apiUrl) {
    try {
      new URL(apiUrl).toURI();
      return false;
    } catch (MalformedURLException e) {
      return true;
    } catch (URISyntaxException e) {
      return true;
    }
  }

}
